const Airtable = require('airtable');

const constants = require('./constants.js');

let base = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY }).base(constants.productionDatabaseKey);
if (process.env.IS_PROD) {
} else {
    //const base = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY }).base(constants.developmentDatabaseKey);
}

module.exports = {
    writeToBase:
        function (baseName, objectToSet) {
            return new Promise(function (response, reject) {
                base(baseName).create(
                    objectToSet,
                    function (err, record) {
                        if (err) { console.error(err); reject(err); return; }
                        record.fields.uniqueid = record.id;
                        response(record.fields);
                    });
            })
        },
    updateRecord:
        function (baseName, recordID, objectToSet) {
            return new Promise(function (response, reject) {
                base(baseName).update(
                    recordID,
                    objectToSet,
                    function (err, record) {
                        if (err) { console.error(err); reject(err); return; }
                        record.fields.uniqueid = record.id;
                        response(record.fields);
                    });
            })
        },
    retrieveRecord:
        function (baseName, recordID) {
            return new Promise(function (response, reject) {
                base(baseName).find(recordID, function (err, record) {
                    if (err) { console.error(err); reject(err); return; }
                    record.fields.uniqueid = record.id;
                    response(record.fields);
                })
            })
        },
    listAllRecords:
        function (baseName) {
            return new Promise(function (response, reject) {
                let recordList = [];
                base(baseName).select({
                    view: "Grid view"
                }).eachPage(function page(records, fetchNextPage) {
                    records.forEach(function (record) {
                        record.fields.uniqueid = record.id;
                        recordList.push(record.fields);
                    });
                    response(recordList);
                    fetchNextPage();
                }, function done(err) {
                    if (err) { reject(err); return; }
                });
            })
        },
    findRecordsByParams:
        function (baseToFetch, fieldsToFetchArray, formulaToFilterStr, maxRecordsToFetchNum, sortByObj, viewToUseStr) {
            return new Promise(function (response, reject) {
                base(baseToFetch).select(
                    {
                        maxRecords: maxRecordsToFetchNum,
                        filterByFormula: formulaToFilterStr.toString(),
                        view: viewToUseStr,
                        fields: fieldsToFetchArray,
                        sort: sortByObj
                    }
                ).eachPage(
                    function page(records, fetchNextPage) {
                        if (records.length === 0) {
                            reject("None found")
                        }
                        records.forEach(function (record) {
                            record.fields.uniqueid = record.id;
                            response(record.fields);
                        });
                        fetchNextPage();
                    }, function done(err) {
                        if (err) { console.error("error finding" + err); reject(err); return false; }
                    }
                )
            })
        },
}
