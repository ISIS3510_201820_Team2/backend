const express = require('express');

const Market = require('../model/Market');

const router = express.Router();

router.get('/', (req, res, next) => {
    Market.getAllMarkets()
        .then(markets => res.json(markets))
        .catch(err => res.status(500).json(err))
})

router.post('/', (req, res) => {
    const { image, location, lat, long, name } = req.body;
    Market.createMarket(image, location, lat, long, name)
        .then(newRecord => res.json(newRecord))
        .catch(err => res.status(500).json(err))
}) 

router.get('/:marketid', (req, res) => {
    Market.getMarket(req.params.marketid)
        .then(market => res.json(market))
        .catch(err => res.status(500).json(err))
})

module.exports = router