const express = require('express');

const Stand = require('../model/Stand');

const router = express.Router();

router.get('/', (req, res, next) => {
    Stand.getAllStands()
        .then(stands => res.json(stands))
        .catch(err => res.status(500).json(err))
})

router.post('/', (req, res) => {
    const { id, productsArray, market } = req.body;
    Stand.createStand(id, productsArray, market)
        .then(newRecord => res.json(newRecord))
        .catch(err => res.status(500).json(err))
})


router.get('/:standid', (req, res) => {
    Stand.getStand(req.params.standid)
        .then(stand => res.json(stand))
        .catch(err => res.status(500).json(err))
})

router.post('/modifyproducts', (req, res) => {
    const { standId, newProducts } = req.body;
    Stand.modifyStand(standId, { "Products": newProducts })
        .then(modifiedStand => res.json(modifiedStand))
        .catch(err => res.status(500).json(err))
})

module.exports = router