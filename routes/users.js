const express = require('express');

const User = require('../model/User');

const router = express.Router();

router.get('/', (req, res, next) => {
    User.getAllUsers()
        .then(users => res.json(users))
        .catch(err => res.status(500).json(err))
})

router.post('/', (req, res) => {
    const { email, name, lastname, phone, language, gender, nationality, password } = req.body;
    User.createUser(email, name, lastname, phone, language, gender, nationality, password)
        .then(newRecord => res.json(newRecord))
        .catch(err => res.status(500).json(err))
})

router.get('/login', (req, res) => {
    User.loginUser(req.body.email, req.body.password)
        .then(user => res.json(user))
        .catch(err => res.status(500).json(err))
})

module.exports = router
