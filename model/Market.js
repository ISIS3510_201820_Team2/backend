const airtable = require('../airtable');

const MARKET_BASE = 'Markets'

const public = {};

public.createMarket = (imagesInfo, location, lat, long, name) => {
    return new Promise((resolve, reject) => {
        airtable.writeToBase(MARKET_BASE, {
            'Image': imagesInfo,
            'Location': location,
            'Lat': lat,
            'Long': long,
            'Name': name,
        }).then(record => resolve(record))
            .catch(err => {
                console.log(err);
                reject(err);
            })
    });
}

public.getAllMarkets = () => {
    return new Promise((resolve, reject) => {
        airtable.listAllRecords(MARKET_BASE)
            .then(markets => resolve(markets))
            .catch(err => {
                console.log(err);
                reject(err);
            })
    })
}

public.getMarket = (recordID) => {
    return new Promise((resolve, reject) => {
        airtable.retrieveRecord(MARKET_BASE, recordID)
            .then(market => resolve(market))
            .catch(err => {
                console.log(err);
                reject(err);
            })
    })
}
module.exports = public;