const airtable = require('../airtable');

const STAND_BASE = 'Stands'

const public = {};

public.createStand = (id, productsArray, market) => {
    return new Promise((resolve, reject) => {
        airtable.writeToBase(STAND_BASE, {
            'ID': id,
            'Products': productsArray,
            'Market': market,
        }).then(record => resolve(record))
            .catch(err => {
                console.log(err);
                reject(err);
            })
    });
}

public.getAllStands = () => {
    return new Promise((resolve, reject) => {
        airtable.listAllRecords(STAND_BASE)
            .then(stands => resolve(stands))
            .catch(err => {
                console.log(err);
                reject(err);
            })
    })
}

public.getStand = (recordID) => {
    return new Promise((resolve, reject) => {
        airtable.retrieveRecord(STAND_BASE, recordID)
            .then(stand => resolve(stand))
            .catch(err => {
                console.log(err);
                reject(err);
            })
    })
}

public.modifyStand = (standID, modifyObject) => {
    return new Promise((resolve, reject) => {
        airtable.updateRecord(STAND_BASE, standID, modifyObject)
            .then(modifiedStand => resolve(modifiedStand))
            .catch(err => {
                console.log(err);
                reject(err);
            })
    })
}

module.exports = public;