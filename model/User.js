const airtable = require('../airtable');
const security = require('../functions/security');

const USER_BASE = 'Users'

const public = {};

public.createUser = (email, name, lastname, phone, language, gender, nationality, password) => {
    let creds = security.generateCredentials(password);
    return new Promise((resolve, reject) => {
        airtable.writeToBase(USER_BASE, {
            "Email": email.toString(),
            "Name": name.toString(),
            "Lastname": lastname.toString(),
            "Gender": gender.toString(),
            "Nationality": nationality.toString(),
            "Language": language.toString(),
            "Phone": phone.toString(),
            "Salt": creds.salt,
            "Hash": creds.hash,
        })
            .then(record =>
                resolve(security.generateToken(record[0], record[1].email))
            )
            .catch(err => {
                console.log(err);
                reject(err);
            })
    });
}

public.loginUser = (email, password) => {
    return new Promise((resolve, reject) => {
        airtable.findRecordsByParams(USER_BASE,
            ["Email", "Salt", "Hash", "Language", "Name", "Lastname", "Gender", "Nationality", "Phone"],
            "Email=" + email, 1, {}, "Grid view")
            .then(user => {
                if (security.verifyPassword(user[1].Hash, user[1].Salt, password)) {
                    resolve(security.generateToken(user[0], user[1].email))
                } else {
                    resolve("Wrong Password or Email")
                }
            })
            .catch(err => {
                console.log(err);
                reject(err);
            })
    })
}

public.getUser = (recordID) => {
    return new Promise((resolve, reject) => {
        airtable.retrieveRecord(USER_BASE, recordID)
            .then(user => resolve(user))
            .catch(err => {
                console.log(err);
                reject(err);
            })
    })
}
module.exports = public;
